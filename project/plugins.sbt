addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.3")

// Coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

addSbtPlugin("com.jamesward" %% "play-auto-refresh" % "0.0.15")
