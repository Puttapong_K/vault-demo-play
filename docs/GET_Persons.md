# Persons

## Get Persons

Call: `GET /api/{version}/persons`

with header `Content-Type: application/json`

e.g. `/api/v1/persons`

---

### Response

JSON of the Persons
The default structure of the result objects response

```javascript
{
    "status_code": Int,
    "data": [
        {
            "id": String,
            "name": String,
            "age": Int,
            "address": String,
            "phone_number": Array[String]
        },
        {
            "id": String,
            "name": String,
            "age": Int,
            "address": String,
            "phone_number": Array[String]
        }
    ]
}
```

#### Example

```javascript
{
    "status_code": 200,
    "data": [
        {
            "id": "id::4f37b506-94c0-4b1f-b368-d66d2011886c",
            "name": "Puttapong Khemcharoen",
            "age": 23,
            "address": "2vrCk51qUEAaovLmBN0s0wv3KyplSTj/emmBpOb29GU=",
            "phone_number": "0944396999"
        },
        {
            "id": "id::579086f7-afc4-4f6c-9663-724838292ed8",
            "name": "Mr. Scala Scale360",
            "age": 23,
            "address": "2vrCk51qUEAaovLmBN0s0wv3KyplSTj/emmBpOb29GU=",
            "phone_number": "212224236"
        }
    ]
}
```

---

### Failure

In the case failures, such as invalid request body, all filed are require.
The server will return list of error messages.

```javascript
{
    "status_code": Int,
    "message": String,
    "info": String
}
```
#### Example

```
{
    "status_code": 404,
    "message": "Not found any persons in database",
    "info": ""
}

```
