# Persons

## Get Person

Call: `GET /api/{version}/persons/{id}`

with header `Content-Type: application/json`

e.g. `/api/v1/persons/id::4f37b506-94c0-4b1f-b368-d66d2011886c`

---

### Response

JSON of the Person
The default structure of the result object response

```javascript
{
    "status_code": Int,
    "data": {
        "id": String,
        "name": String,
        "age": Int,
        "address": String,
        "phone_number": String
    }
}
```

#### Example

```javascript
{
    "status_code": 200,
    "data": {
        "id": "id::4f37b506-94c0-4b1f-b368-d66d2011886c",
        "name": "Auzanaruk",
        "age": 23,
        "address": "2vrCk51qUEAaovLmBN0s0wv3KyplSTj/emmBpOb29GU=",
        "phone_number": "02345"
    }
}
```

---

### Failure

In the case failures, such as invalid request body, all filed are require.
The server will return list of error messages.

```javascript
{
    "status_code": Int,
    "message": String,
    "info": String
}
```
#### Example

```
{
    "status_code": 404,
    "message": "Not found a person by person_id: 17b9d1b5-c2b9-4602-8fc7-2063add20d9d",
    "info": ""
}
```
