# Person

## Create Person

Call: `POST /api/{version}/persons`

with header `Content-Type: application/json`

e.g. `/api/v1/persons`

### POST body format (Create Person)

Requires a JSON object with these fields for create person.

|Name|Type|Description|Required|
|---|---|---|---|
|name|String|A person name|Yes
|age|Int|Age of person|Yes
|address|String|Your local address|Yes
|phone_number|String|Your telephone number|Yes

```javascript
{
    "name": String,
    "age": Int,
    "address": String,
    "phone_number": String
}
```

#### Example

```javascript
{
    "name": "Puttapong Khemcharoen",
    "age": 23,
    "address": "Bangkok",
    "phone_Number": "0944396999"
}
```

---

### Response

```javascript
{
  "status_code": Int,
  "data": {
    "id": String,
    "name": String,
    "age": Int,
    "address": String,
    "phone_number": String
  }
}
```

#### Example

```javascript
{
    "status_code": 201,
    "data": {
        "id": "id::6942e201-418f-4ce8-912b-3f3703980634",
        "name": "vault:v1:glFa8XigNiWyPAgdAtScMO+eaQDSzA/VnOkVUJDlss/96BF8hQ==",
        "age": 23,
        "address": "2vrCk51qUEAaovLmBN0s0wv3KyplSTj/emmBpOb29GU=",
        "phone_number": "0944396999"
    }
}
```

##### Remark

- "id" will auto generate from random UUID
- "name" will auto encryption data to Cipher text
- "address" will auto Hash the data by Algorithm

---

### Failure

In the case failures, such as invalid request body, all filed are require.
The server will return list of error messages.

```javascript
{
    "status_code": Int,
    "message": String,
    "info": String
}
```
#### Example

```
{
    "status_code": 400,
    "message": "Can not covert Json body",
    "info": ""
}

```
