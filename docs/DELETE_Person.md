# Person

## Delete Person

Call: `DELETE /api/{version}/persons/{id}`

e.g. `/api/v1/persons/id::4f37b506-94c0-4b1f-b368-d66d2011886c`

---

### Response

```javascript
{
    "status_code": Int,
    "data": String
}
```

#### Example

```javascript
{
    "status_code": 200,
    "data": "Success"
}
```

##### Remark

- Delete Person will auto remove vault data

---

### Failure

In the case failures, such as invalid request body, all filed are require.
The server will return list of error messages.

```javascript
{
    "status_code": Int,
    "message": String,
    "info": String
}
```
#### Example

```
{
    "status_code": 404,
    "message": "Not found a person by person_id: 17b9d1b5-c2b9-4602-8fc7-2063add20d9d",
    "info": ""
}
```
