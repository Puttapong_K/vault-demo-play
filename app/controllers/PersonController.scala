package controllers

import javax.inject.Inject

import core.Response
import models._
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import services.PersonService

import scala.concurrent.{ExecutionContext, Future}

class PersonController @Inject()(val controllerComponents: ControllerComponents
                                 , val personService: PersonService)(implicit ec: ExecutionContext)
  extends BaseController
    with Response {

  implicit val personResponse = Json.format[PersonResp]

  def createPerson: Action[JsValue] = Action.async(parse.json) { request =>
    val personReq: Either[String, PersonRequest] = request.body.validate[PersonRequest].asOpt match {
      case Some(personRequest) => Right(personRequest)
      case None => Left(buildFailureFormatJsString(BAD_REQUEST, Errors(BAD_REQUEST, "Can not covert Json body", None)))
    }

    personReq match {
      case Right(personRequest) => personService.createPerson(personRequest) map {
        case Right(person) =>
          Created(buildSuccessFormatJsString(CREATED, person))
        case Left(errorData) =>
          val errors = Errors(errorData.status_code.intValue(), errorData.errors, errorData.info)
          Status(errors.code)(buildFailureFormatJsString(errors.code, errors))
      }
      case Left(errorJSFormat) => Future.successful(Status(BAD_REQUEST)(errorJSFormat))
    }
  }

  def findAllPersons: Action[AnyContent] = Action.async { implicit rs =>
    personService.findAllPersons map {
      case Right(persons) => Ok(buildSuccessFormatJsString(OK, persons))
      case Left(error) =>
        val errors: Errors[Object] = Errors(error.status_code.intValue(), error.errors.mkString(","), error.info.getOrElse(None))
        Status(error.status_code.intValue())(buildFailureFormatJsString(error.status_code.intValue(), errors))
    }
  }

  def findPersonById(personId: String): Action[AnyContent] = Action.async { implicit rs =>
    personService.findPersonById(personId) map {
      case Right(persons) => Ok(buildSuccessFormatJsString(OK, persons))
      case Left(error) =>
        val errors = Errors(error.status_code.intValue(), error.errors.mkString(","), error.info)
        Status(errors.code)(buildFailureFormatJsString(errors.code, errors))
    }
  }

  def deletePersonById(personId: String): Action[AnyContent] = Action.async {
    personService.deletePerson(personId) map {
      case Right(deleteSuccess) => Status(deleteSuccess._2.intValue())(buildSuccessFormatJsString(deleteSuccess._2.intValue(), deleteSuccess))
      case Left(error) =>
        val errors = Errors(error.status_code.intValue(), error.errors.mkString(","), error.info)
        Status(errors.code)(buildFailureFormatJsString(errors.code, errors))
    }
  }

}
