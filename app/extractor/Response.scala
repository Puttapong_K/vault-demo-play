package extractor

import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc.{Result, Results}

trait Response extends Results {

  import FailureResponse.failureResponseFormatWrites
  import SuccessResponse.successResponseFormatWrite

  implicit class WriteToJson[W: Writes](w: W) {
    def toJsValue: JsValue = Json.toJson(w)
  }

  def buildSuccess(status_code: Int, data: SuccessResponse): Result = Status(status_code)(data.toJsValue)

  def buildFailure(status_code: Int, error: FailureResponse): Result = Status(status_code)(error.toJsValue)

}