package extractor

import akka.http.scaladsl.model.StatusCode
import play.api.libs.json.{JsValue, Json, OFormat, OWrites}
import play.api.mvc.Result
case class FailureResponse(status_code: Int, message: String, info: Option[JsValue]) extends Response {



  def response: Result = buildFailure(status_code, this)
}


object FailureResponse {

  def apply(status_code: StatusCode, message: String, info: Option[JsValue]) = new FailureResponse(status_code.intValue(), message, info)

  def apply(status_code: StatusCode, message: Seq[String], info: Option[JsValue]) = new FailureResponse(status_code.intValue(), message.mkString(", "), info)

  implicit val failureResponseFormat: OFormat[FailureResponse] = Json.format[FailureResponse]


  implicit val failureResponseFormatWrites: OWrites[FailureResponse] = Json.writes[FailureResponse]

}