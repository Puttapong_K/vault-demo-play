package extractor

import akka.http.scaladsl.model.StatusCode
import play.api.libs.json._
import play.api.mvc.{Result, Results}

import scala.language.implicitConversions

case class SuccessResponse(status_code: Int, data: JsValue) extends Response {
  def response: Result = buildSuccess(status_code, this)
}

object SuccessResponse {

  def apply(status_code: StatusCode, data: JsValue): SuccessResponse = new SuccessResponse(status_code.intValue(), data)

  implicit val successResponseFormat: OFormat[SuccessResponse] = Json.format[SuccessResponse]

  implicit val successResponseFormatWrite: OWrites[SuccessResponse] = Json.writes[SuccessResponse]
}


