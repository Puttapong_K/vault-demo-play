package exception

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import play.api.libs.json.JsValue

abstract class ErrorException(val status_code: StatusCode, val errors: Seq[String], val info: Option[JsValue] = None) extends Exception {
}

case class BadRequestException(status: StatusCode = StatusCodes.BadRequest, errorMsg: Seq[String], infomation: Option[JsValue] = None) extends ErrorException(status, errorMsg, infomation)

case class NotFoundException(status: StatusCode = StatusCodes.NotFound, errorMsg: Seq[String], infomation: Option[JsValue] = None) extends ErrorException(status, errorMsg, infomation)

case class InternalServerErrorException(status: StatusCode = StatusCodes.InternalServerError, errorMsg: Seq[String], infomation: Option[JsValue] = None) extends ErrorException(status, errorMsg, infomation)

case class ServiceUnavailableException(status: StatusCode = StatusCodes.ServiceUnavailable, errorMsg: Seq[String], infomation: Option[JsValue] = None) extends ErrorException(status, errorMsg, infomation)

case class ErrorMessage(status: StatusCode, errorMsg: String, information: Option[JsValue])


