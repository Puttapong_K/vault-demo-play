package models

case class Vault(id: String
                 , contextId: String
                 , hmac: String)