package models

object UUID {
  def generateUUID(str: String): String = s"${str.toLowerCase}:${java.util.UUID.randomUUID()}"
}