package models

import com.scale360.vaultclient.VaultConfig
import com.scale360.vaultclient.auth.{AppRoleCredential, UserPassCredential}
import com.scale360.vaultclient.client.ScalajHttpClient
import com.scale360.vaultclient.secret.transit.KeyName

class VaultConfiguration(val server: VaultConfig
                         , val credentialByLogin: UserPassCredential
                         , val credentialAppRole: AppRoleCredential
                         , val key: KeyName) {
  val client: ScalajHttpClient = new ScalajHttpClient(server)
}

object VaultConfiguration {

  def apply(server: VaultConfig
            , credentialByLogin: UserPassCredential
            , credentialAppRole: AppRoleCredential
            , keyName: KeyName): VaultConfiguration = new VaultConfiguration(server, credentialByLogin, credentialAppRole, keyName)

  def apply(server: VaultConfig
            , credentialByLogin: UserPassCredential
            , keyName: KeyName): VaultConfiguration = new VaultConfiguration(server, credentialByLogin, AppRoleCredential(AppRoleCredential.RoleId(""), AppRoleCredential.SecretId("")), keyName)

  def apply(server: VaultConfig
            , credentialAppRole: AppRoleCredential
            , keyName: KeyName): VaultConfiguration = new VaultConfiguration(server, UserPassCredential("", ""), credentialAppRole, keyName)
}