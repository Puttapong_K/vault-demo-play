package models

import play.api.libs.json.{Json, OFormat}


case class Person(personId: String
                  , name: String
                  , age: Int
                  , address: String
                  , phone_number: String) {

  def toPersonResp = PersonResp(id = personId
    , name = name
    , age = age
    , address = address
    , phone_number = phone_number)

  def toPersonsResp = Seq(toPersonResp)
}


case class PersonRequest(name: String
                         , age: Int
                         , address: String
                         , phone_number: String) {
  def toPerson: Person = Person(personId = UUID.generateUUID("id:")
    , name = name
    , age = age
    , address = address
    , phone_number = phone_number)
}

object PersonRequest {
  implicit val personRequestFormat: OFormat[PersonRequest] = Json.format[PersonRequest]
}


case class PersonResp(id: String
                      , name: String
                      , age: Int
                      , address: String
                      , phone_number: String)

object PersonResp {
  implicit val personResponseFormat: OFormat[PersonResp] = Json.format[PersonResp]

}