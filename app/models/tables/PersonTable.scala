package models.tables


import javax.inject.Inject

import models.Person
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile


class PersonTable @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig.profile.api._

  class Persons(tag: Tag) extends Table[Person](tag, "person") {

    def person_id: Rep[String] = column[String]("person_id", O.PrimaryKey)

    def name: Rep[String] = column[String]("name")

    def age: Rep[Int] = column[Int]("age")

    def address: Rep[String] = column[String]("address")

    def phone_number: Rep[String] = column[String]("phone_number")

    def * = (person_id, name, age, address, phone_number) <> (Person.tupled, Person.unapply)
  }

  val Persons: TableQuery[Persons] = TableQuery[Persons]
}