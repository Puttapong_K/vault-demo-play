package models.tables

import javax.inject.Inject

import models.Vault
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape


class VaultTable @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._


  class Vaults(tag: Tag) extends Table[Vault](tag, "vault") {

    def id: Rep[String] = column[String]("vault_id", O.PrimaryKey)

    def contextId: Rep[String] = column[String]("contextId")

    def hmac: Rep[String] = column[String]("hmac")


    def * : ProvenShape[Vault] = (id, contextId, hmac) <> (Vault.tupled, Vault.unapply)
  }

  val Vaults: TableQuery[Vaults] = TableQuery[Vaults]
}
