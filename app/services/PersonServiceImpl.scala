package services

import javax.inject.Inject

import com.scale360.vaultclient
import com.scale360.vaultclient.VaultError
import com.scale360.vaultclient.secret.transit._
import dao.{PersonDao, VaultDao}
import exception.{BadRequestException, ErrorException, InternalServerErrorException}
import models.{Person, PersonRequest, Vault}
import slick.dbio.{DBIOAction, Effect, NoStream}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class PersonServiceImpl @Inject()(personDao: PersonDao, val vaultService: VaultService, vaultDao: VaultDao) extends PersonService {


  private def findVaultsByIds(personIds: Seq[String]): Future[Either[ErrorException, Seq[Vault]]] = vaultDao.findVaultsByIds(personIds)

  private def findVaultById(personId: String): Future[Either[ErrorException, Vault]] = vaultDao.findVaultById(personId)

  private def decryptPerson(person: Person): Future[Either[ErrorException, Person]] =
    findVaultById(person.personId) map {
      case Right(vault) =>
        val ctInput = vaultService.makeCipherTextInputWithContext(vault.contextId, person.name)
        vaultService.decryptCipherText(ctInput) match {
          case Right(plaintext) => Right(person.copy(name = new String(plaintext.bytes, "utf-8")))
          case Left(vaultError) => Left(InternalServerErrorException(errorMsg = Seq(vaultError.toString)))
        }
      case Left(errorException) => Left(errorException)
    }

  private def verifyPhoneNumbers(persons: Seq[Person]): Future[Either[ErrorException, Boolean]] =
    findVaultsByIds(persons.map(_.personId)) map {
      case Right(vaults) =>
        val result = persons flatMap { person =>
          vaults.filter(_.id == person.personId) map { vault =>
            vaultService.verifyHMAC(person.phone_number, Hmac(vault.hmac))
          }
        }
        result.partition(_.isLeft) match {
          case (Nil, success) => Right(success flatMap (_.right.toOption) forall identity)
          case (failure, _) => Left(InternalServerErrorException(errorMsg = failure.flatMap(_.left.toOption).map(_.toString)))
        }
      case Left(errorException) => Left(errorException)
    }


  private def verifyPhoneNumber(person: Person): Future[Either[ErrorException, Boolean]] =
    findVaultById(person.personId) map {
      case Right(vault) =>
        vaultService.verifyHMAC(person.phone_number, Hmac(vault.hmac)) match {
          case Right(status) =>
            if (status) Right(true) else Left(InternalServerErrorException(errorMsg = Seq("Invalid phone number data had changed.")))
          case Left(vaultError) => Left(InternalServerErrorException(errorMsg = Seq(vaultError.toString)))
        }
      case Left(errorException) => Left(errorException)
    }

  private def decryptPersons(persons: Seq[Person]): Future[Either[ErrorException, Seq[Person]]] =
    findVaultsByIds(persons map (_.personId)) map {
      case Right(vaults) =>
        val tupleData: Seq[(Vault, Person)] = vaults zip persons
        val seqCT: Seq[CiphertextInput] = tupleData map (item => vaultService.makeCipherTextInputWithContext(item._1.contextId, item._2.name))
        val result: Either[VaultError, vaultclient.Seq[Plaintext]] = vaultService.decryptBatchCipherText(seqCT.to[collection.immutable.Seq])
        result match {
          case Right(seqPlaintext) =>
            val tuplePersonAndPlainText = persons zip seqPlaintext
            Right(tuplePersonAndPlainText map { item => item._1.copy(name = new String(item._2.bytes)) })
          case Left(vaultError) => Left(InternalServerErrorException(errorMsg = Seq(vaultError.toString)))
        }
      case Left(error) => Left(error)
    }


  def encryptPerson(person: Person): Either[VaultError, (Vault, Person)] = {
    val results: Either[VaultError, ((vaultService.contextId, Ciphertext), Hash, Hmac)] = for {
      cipherTextName <- vaultService.encryptData(person.name)
      hashAddress <- vaultService.hashData(person.address)
      hmacPhone <- vaultService.hashMacData(person.phone_number)
    } yield (cipherTextName, hashAddress, hmacPhone)
    results match {
      case Right(secretData) => Right(
        (
          // Part one Vault information for vault table
          Vault(id = person.personId
            , contextId = secretData._1._1
            , hmac = secretData._3.value
          ),
          // Part two Person information for person table
          Person(
            personId = person.personId
            , name = secretData._1._2.value
            , age = person.age
            , address = secretData._2.toBase64.toString
            , phone_number = person.phone_number)
        ))
      case Left(vaultError) => Left(vaultError)
    }
  }


  override def createPerson(personReq: PersonRequest): Future[Either[ErrorException, Person]] = {
    val person: Person = personReq.toPerson
    val vaultAndPerson: Either[VaultError, (Vault, Person)] = encryptPerson(person)
    vaultAndPerson match {
      case Right(secretData) =>
        val action: DBIOAction[Person, NoStream, Effect.Write with Effect.Write with Effect] =
          vaultDao.createVaultAction(secretData._1) zip personDao.createPersonAction(secretData._2) flatMap {
            case (vault, person) => DBIOAction.successful(secretData._2)
            case _ => DBIOAction.failed(BadRequestException(errorMsg = Seq("error to insert data to database")))
          }
        personDao.runWithTransaction(action) map {
          case Left(error) => Left(error)
          case Right(data) => Right(data)
        }
      case Left(vaultError) => Future.successful(Left(InternalServerErrorException(errorMsg = Seq(vaultError.toString))))
    }
  }

  override def findAllPersons: Future[Either[ErrorException, Seq[Person]]] =
    personDao.findAllPersons flatMap {
      case Right(persons) =>
        val verifiedResult: Future[Either[ErrorException, Boolean]] = verifyPhoneNumbers(persons)
        val decryptionResult: Future[Either[ErrorException, Seq[Person]]] = decryptPersons(persons)
        verifiedResult flatMap {
          case Right(success) => decryptionResult
          case Left(error) => Future.successful(Left(error))
        }
      case Left(error) =>
        Future.successful(Left(error))
    }


  override def findPersonById(id: String): Future[Either[ErrorException, Person]] =
    personDao.findPersonById(id) flatMap {
      case Right(person) =>
        verifyPhoneNumber(person) flatMap {
          case Right(success) => decryptPerson(person)
          case Left(error) => Future.successful(Left(error))
        }
      case Left(errorException) => Future.successful(Left(errorException))
    }


  override def deletePerson(id: String): Future[Either[ErrorException, DeleteSuccess]] =
    for {
      person <- personDao.deletePerson(id)
      _ <- vaultDao.deleteVaultById(id)
    } yield person

}
