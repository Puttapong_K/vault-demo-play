package services

import javax.inject.Inject

import com.scale360.vaultclient.auth._
import com.scale360.vaultclient.secret.transit._
import com.scale360.vaultclient.tokenstore.{AutoReauthenTokenStore, DynamicTokenStoreConfig, DynamicTokenStoreHandler}
import com.scale360.vaultclient.{Seq, VaultConfig, VaultError, VaultToken}
import com.typesafe.config.Config
import models.VaultConfiguration


class VaultServiceImpl @Inject()(config: Config) extends VaultService {

  // Get vault config details from Application
  private val server = config.getString("vault.server")
  private val version: Int = config.getInt("vault.version")
  private val user = config.getString("vault.user")
  private val password = config.getString("vault.password")
  private val roleId = config.getString("vault.roleid")
  private val secretId = config.getString("vault.secretid")
  private val keyname = KeyName(config.getString("vault.keyname"))

  val hashAlgorithm = HashAlgorithm.SHA2_256

  // setting vault
  private val vaultSettings: VaultConfiguration = VaultConfiguration.apply(server = VaultConfig(server, version)
    , credentialByLogin = UserPassCredential(user, password)
    , credentialAppRole = AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId))
    , keyName = keyname)

  implicit val appRole: AppRole = new AppRole(vaultSettings.client)

  implicit val userPass: UserPass = new UserPass(vaultSettings.client)

  private val tokenStoreConfig: DynamicTokenStoreConfig = DynamicTokenStoreConfig.Defaults

  private val tokenStoreHandle: DynamicTokenStoreHandler = DynamicTokenStoreHandler(
    handleError = error => println("error!" + error.getMessage),
    reportPermanentToken = accessor => println("permanent token detected" + accessor.toString)
  )


  val transit: TransitWithTokenStore = transitWithTokenStore(tokenStore)


  private def toPlainText(data: String): Plaintext = Plaintext(data.getBytes())

  private def generateContextId: String = java.util.UUID.randomUUID.toString

  private def makeContext(contextId: String): Context = Context(contextId.getBytes)


  private def makePlainTextInput(plaintext: Plaintext, context: Context): PlaintextInput = plaintext.toInput.withContext(context)


  override def authorizedUser(userPassCredential: UserPassCredential): Either[VaultError, AuthResponse] = userPass.authenticate(userPassCredential)

  override def authorizedByRole(appRoleCredential: AppRoleCredential): Either[VaultError, AuthResponse] = appRole.authenticate(appRoleCredential)

  override def transitWithToken(token: VaultToken): TransitWithTokenFn = new TransitWithTokenFn(vaultSettings.client, () => token)

  override def tokenStore: AutoReauthenTokenStore[UserPassCredential] = AutoReauthenTokenStore(vaultSettings.credentialByLogin, tokenStoreConfig, tokenStoreHandle)


  override def transitWithTokenStore(tokenStore: AutoReauthenTokenStore[UserPassCredential]): TransitWithTokenStore = new TransitWithTokenStore(vaultSettings.client, tokenStore)


  override def makeCipherTextInputWithContext(contextKey: String, encryptData: String): CiphertextInput = Ciphertext(encryptData).toInput.withContext(Context(contextKey.getBytes))


  override def decryptCipherText(ctInput: CiphertextInput, keyName: KeyName = vaultSettings.key): Either[VaultError, Plaintext] = transit.decrypt(vaultSettings.key)(ctInput)

  override def decryptBatchCipherText(seqCT: Seq[CiphertextInput], keyName: KeyName = vaultSettings.key): Either[VaultError, Seq[Plaintext]] = {
    transit.decryptBatch(keyName)(seqCT).right.flatMap(item => item.sequencedVaultError) match {
      case Right(data) => Right(data)
      case Left(error) => Left(error)
    }
  }

  override def encryptData(data: String, keyName: KeyName = vaultSettings.key): Either[VaultError, (String, Ciphertext)] = {
    val contextId = generateContextId
    val context = makeContext(contextId)
    encryptionPlainText(toPlainText(data), context, keyName) match {
      case Right(cipherText) => Right((contextId, cipherText))
      case Left(error) => Left(error)
    }
  }

  override def hashData(data: String): Either[VaultError, Hash] =
    transit.hash(hashAlgorithm)(toPlainText(data))



  override def hashMacData(data: String): Either[VaultError, Hmac] =
    transit.hmac(vaultSettings.key.toHmacParam)(toPlainText(data))


  override def verifyHMAC(data: String, hmac: Hmac): Either[VaultError, Boolean] = transit.verifyHmac(vaultSettings.key.toHmacParam)(toPlainText(data), hmac)

  override def encryptionPlainText(plaintext: Plaintext, context: Context, keyName: KeyName = vaultSettings.key): Either[VaultError, Ciphertext] = transit.encrypt(keyName)(makePlainTextInput(plaintext, context))

  override def encryptionBatchPlainText(plaintextWithContexts: Seq[(Plaintext, Context)], keyName: KeyName = vaultSettings.key): Either[VaultError, BatchResult[Ciphertext]] =
    transit.encryptBatch(keyName)(plaintextWithContexts.map(plaintextWithContext => makePlainTextInput(plaintextWithContext._1, plaintextWithContext._2)))

  override def renewCipherText(cipherTextInput: CiphertextInput): Either[VaultError, Ciphertext] = transit.rewrap(vaultSettings.key)(cipherTextInput)
}


