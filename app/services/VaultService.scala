package services

import com.google.inject.ImplementedBy
import com.scale360.vaultclient.auth.{AppRoleCredential, AuthResponse, UserPassCredential}
import com.scale360.vaultclient.secret.transit._
import com.scale360.vaultclient.tokenstore.AutoReauthenTokenStore
import com.scale360.vaultclient.{Seq, VaultError, VaultToken}


@ImplementedBy(classOf[VaultServiceImpl])
trait VaultService {


  type contextId = String

  def authorizedUser(userPassCredential: UserPassCredential): Either[VaultError, AuthResponse]

  def transitWithToken(token: VaultToken): TransitWithTokenFn

  def tokenStore: AutoReauthenTokenStore[UserPassCredential]

  def authorizedByRole(appRoleCredential: AppRoleCredential): Either[VaultError, AuthResponse]

  def transitWithTokenStore(tokenStore: AutoReauthenTokenStore[UserPassCredential]): TransitWithTokenStore

  def decryptCipherText(ct: CiphertextInput, keyName: KeyName = KeyName("")): Either[VaultError, Plaintext]

  def decryptBatchCipherText(seqCT: Seq[CiphertextInput], keyName: KeyName = KeyName("")): Either[VaultError, Seq[Plaintext]]

  def verifyHMAC(data: String, dataHmac: Hmac): Either[VaultError, Boolean]

  def makeCipherTextInputWithContext(encryptData: String, contextKey: String): CiphertextInput

  def encryptData(data: String, keyName: KeyName = KeyName("")): Either[VaultError, (contextId, Ciphertext)]

  def encryptionPlainText(plaintext: Plaintext, context: Context, keyName: KeyName = KeyName("")): Either[VaultError, Ciphertext]

  def encryptionBatchPlainText(plaintextWithContexts: Seq[(Plaintext, Context)],keyName: KeyName = KeyName("")): Either[VaultError, BatchResult[Ciphertext]]

  def renewCipherText(cipherTextInput: CiphertextInput): Either[VaultError, Ciphertext]


  def hashData(data: String): Either[VaultError, Hash]

  def hashMacData(data: String): Either[VaultError, Hmac]
}