package services

import akka.http.scaladsl.model.StatusCode
import com.google.inject.ImplementedBy
import exception.ErrorException
import models.{Person, PersonRequest}

import scala.concurrent.Future

@ImplementedBy(classOf[PersonServiceImpl])
trait PersonService {
  type DeleteSuccess = (String, StatusCode, String)

  def createPerson(personReq: PersonRequest): Future[Either[ErrorException, Person]]

  def findAllPersons: Future[Either[ErrorException, Seq[Person]]]

  def findPersonById(id: String): Future[Either[ErrorException, Person]]

  def deletePerson(id: String): Future[Either[ErrorException, DeleteSuccess]]
}
