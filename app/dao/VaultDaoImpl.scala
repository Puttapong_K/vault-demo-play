package dao

import javax.inject.Inject

import akka.http.scaladsl.model.StatusCodes
import exception.{ErrorException, InternalServerErrorException, NotFoundException}
import models.Vault
import models.tables.VaultTable
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.meta.MTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class VaultDaoImpl @Inject()(vaultTable: VaultTable)(protected val dbConfigProvider: DatabaseConfigProvider) extends VaultDao {

  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]

  import vaultTable._
  import dbConfig.profile.api._

  val db = dbConfig.db
  val SUCCESS = 1


  override def createVaultAction(vault: Vault): DBIOAction[Int, NoStream, Effect.Write] = Vaults += vault

  override def createVault(vault: Vault): Future[Either[ErrorException, Vault]] =
    db.run(createVaultAction(vault)) map {
      case SUCCESS => Right(vault)
      case _ => Left(InternalServerErrorException(errorMsg = Seq("Can not insert vault data to database")))
    }

  override def findAllVault: Future[Either[ErrorException, Seq[Vault]]] =
    db.run(Vaults.result) map {
      case Nil => Left(NotFoundException(errorMsg = Seq("Not found any vaults")))
      case vaults => Right(vaults)
    }

  override def findVaultsByIds(ids: Seq[String]): Future[Either[ErrorException, Seq[Vault]]] =
    db.run(Vaults.filter(_.id inSetBind ids).result) map {
      case vaults if vaults.length == ids.length => Right(vaults)
      case vaults if vaults.length != ids.length =>
        val missingIds: Seq[String] = ids.filterNot(vaults.contains)
        Left(NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${missingIds.mkString(",")}")))

    }

  override def findVaultById(id: String): Future[Either[ErrorException, Vault]] =
    db.run(Vaults.filter(_.id === id.bind).result.headOption) map {
      case Some(person) => Right(person)
      case None => Left(NotFoundException(errorMsg = Seq(s"Not found vault by Id: $id")))
    }

  override def deleteVaultById(id: String): Future[Either[NotFoundException, DeleteSuccess]] =
    db.run(Vaults.filter(_.id === id.bind).delete) map {
      case SUCCESS => Right(id, StatusCodes.OK, "Success")
      case _ => Left(NotFoundException(errorMsg = Seq(s"Not found vault by Id: $id")))
    }
}
