package dao

import akka.http.scaladsl.model.StatusCode
import com.google.inject.ImplementedBy
import exception.ErrorException
import models.{Person, Vault}
import slick.dbio.{DBIO, DBIOAction, Effect, NoStream}

import scala.concurrent.Future

@ImplementedBy(classOf[VaultDaoImpl])
trait VaultDao {

  type DeleteSuccess = (String, StatusCode, String)

  def createVault(vault: Vault): Future[Either[ErrorException, Vault]]

  def findAllVault: Future[Either[ErrorException, Seq[Vault]]]

  def findVaultById(id: String): Future[Either[ErrorException, Vault]]

  def findVaultsByIds(ids: Seq[String]): Future[Either[ErrorException, Seq[Vault]]]

  def deleteVaultById(personId: String): Future[Either[ErrorException, DeleteSuccess]]

  def createVaultAction(vault: Vault):DBIOAction[Int, NoStream, Effect.Write]
}
