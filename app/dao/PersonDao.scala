package dao

import akka.http.scaladsl.model.StatusCode
import com.google.inject.ImplementedBy
import exception.ErrorException
import models.Person
import slick.dbio.{DBIOAction, Effect, NoStream}

import scala.concurrent.Future

@ImplementedBy(classOf[PersonDaoImpl])
trait PersonDao {

  type DeleteSuccess = (String, StatusCode, String)

  def createPerson(person: Person): Future[Either[ErrorException, Person]]

  def findAllPersons: Future[Either[ErrorException, Seq[Person]]]

  def findPersonById(personId: String): Future[Either[ErrorException, Person]]

  def deletePerson(personId: String): Future[Either[ErrorException, DeleteSuccess]]

  def runWithTransaction[R, S <: NoStream, E <: Effect](action: DBIOAction[R, S, E]): Future[Either[ErrorException, R]]

  def createPersonAction(person: Person): DBIOAction[Int, NoStream, Effect.Write]
}
