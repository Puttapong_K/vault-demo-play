package dao

import javax.inject.Inject

import akka.http.scaladsl.model.StatusCodes
import exception._
import models.Person
import models.tables.PersonTable
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class PersonDaoImpl @Inject()(personTable: PersonTable)(protected val dbConfigProvider: DatabaseConfigProvider) extends PersonDao {

  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]

  import personTable._
  import dbConfig.profile.api._

  val db = dbConfig.db

  final val SUCCESS = 1


  override def runWithTransaction[R, S <: NoStream, E <: Effect](action: DBIOAction[R, S, E]): Future[Either[ErrorException, R]] = {
    val tryResult: Future[Try[R]] = db.run {
      action.transactionally.asTry
    }

    tryResult map {
      case Failure(ex) if ex.isInstanceOf[ErrorException] => Left(BadRequestException(errorMsg = Seq("Can not insert person to database")))
      case Failure(_) => Left(ServiceUnavailableException(errorMsg = Seq("Error database")))
      case Success(r) => Right(r.asInstanceOf[R])
    }


  }


  override def createPersonAction(person: Person): DBIOAction[Int, NoStream, Effect.Write] = Persons += person

  override def createPerson(person: Person): Future[Either[InternalServerErrorException, Person]] =
    db.run(createPersonAction(person)) map {
      case SUCCESS => Right(person)
      case _ => Left(InternalServerErrorException(errorMsg = Seq("Can not insert person to database")))
    }


  override def findAllPersons: Future[Either[NotFoundException, Seq[Person]]] =
    db.run(Persons.result) map {
      case Nil => Left(NotFoundException(errorMsg = Seq("Not found any persons")))
      case persons => Right(persons)
    }


  override def findPersonById(personId: String): Future[Either[ErrorException, Person]] =
    db.run(Persons.filter(p => p.person_id === personId.bind).result.headOption) map {
      case Some(person) => Right(person)
      case None => Left(NotFoundException(errorMsg = Seq(s"Not found a person by person_id: $personId")))
    }


  override def deletePerson(personId: String): Future[Either[ErrorException, DeleteSuccess]] =
    db.run(Persons.filter(p => p.person_id === personId.bind).delete) map {
      case SUCCESS => Right(personId, StatusCodes.OK, "Success")
      case _ => Left(NotFoundException(errorMsg = Seq(s"Not found person by person_id: $personId")))
    }


}
