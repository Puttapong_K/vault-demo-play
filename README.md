# Vault-demo (Play framework)


This project was created for show how to make a sensitive data to secret data
by using Vault Client
[Read more about Vault client](https://bitbucket.org/dotography-code/vault-client)



## GETTING STARTED

### Settings Project

```
Require
- mysql database name vault_demo_db
- setting database crendentials (url, username, password), or vault config in Application.conf
```
To run the project using [sbt](http://www.scala-sbt.org/), start up Play:

```
 sbt run
```

And that's it!

Now go to  http://localhost:9000 to see the running web application..


#### Documents

You can reading the documents about how to use this API from folder `docs` or download
all Postman Script for this project

- Download an API Development Environment from using Postman import go to
the folder tools in the project, then you will see the file name
```
    Vault_Demo.postman_collection.json
```

#### Maintainer

Puttapong Khemcharoen(Ma)
