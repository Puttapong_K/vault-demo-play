package dao


import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import exception.{ErrorException, NotFoundException}
import models.tables.PersonTable
import models.{Person, UUID}
import org.scalatest._
import spec.CustomFeatureSpec
import testhelpers.{EvolutionHelper, Injector}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class PersonDaoSpec extends CustomFeatureSpec with BeforeAndAfterEach {


  override def afterEach(): Unit = {
    EvolutionHelper.clean()
    EvolutionHelper.start()
  }

  trait PersonDaoTest {

    val personTable = Injector.inject[PersonTable]
    val personDaoImpl = Injector.inject[PersonDaoImpl]

    val validPersonId = UUID.generateUUID("id:")

    val validPerson: Person = Person(validPersonId
      , "Ma"
      , 23
      , "Kanchanaburi"
      , "1212312121")

  }

  new PersonDaoTest {
    feature("Create person") {
      scenario("I want to create a person data") {
        Given("A person data")
        val person = validPerson
        When("Create person")
        val result: Future[Either[ErrorException, Person]] = personDaoImpl.createPerson(person)
        whenReady(result) { rs =>
          Then(s"The result should be Right($person)")
          rs.right.get should be(person)
        }
      }
    }

    feature("Get Persons") {
      scenario("I want to looks all persons, but its empty") {
        Given("A expect result")
        val expected = NotFoundException(errorMsg = Seq("Not found any persons"))
        When("Call Get persons")
        val result: Future[Either[ErrorException, Seq[Person]]] = personDaoImpl.findAllPersons
        whenReady(result) { rs =>
          Then("The result should be Left(Not found any persons)")
          rs.left.get should be(expected)
        }
      }

      scenario("I want to looks all persons") {
        Given("A mock up person")
        Await.result(personDaoImpl.createPerson(validPerson), 5.seconds)
        And("A expected result")
        val expected = Seq(validPerson)
        When("Call Get persons")
        val result: Future[Either[ErrorException, Seq[Person]]] = personDaoImpl.findAllPersons
        whenReady(result) { rs =>
          Then("The result should be list of persons")
          rs.right.get should be(expected)
        }
      }
    }

    feature("Get Person by Id") {
      scenario("I want to find a person, but database is empty") {
        Given("A id")
        val id = validPersonId
        And("A expected result")
        val expected = NotFoundException(errorMsg = Seq(s"Not found a person by person_id: $id"))
        When("Call Get person")
        val result: Future[Either[ErrorException, Person]] = personDaoImpl.findPersonById(id)
        whenReady(result) { rs =>
          Then(s"The result should be Not found a person by $id")
          rs.left.get should be(expected)
        }
      }

      scenario("I want to find a person") {
        Given("A mockup person")
        Await.result(personDaoImpl.createPerson(validPerson), 5.seconds)
        When("Get a person")
        val result: Future[Either[ErrorException, Person]] = personDaoImpl.findPersonById(validPersonId)
        whenReady(result) { rs =>
          Then("The result should be Right(A Person)")
          rs.right.get should be(validPerson)
        }
      }
    }

    feature("Delete a person") {
      scenario("I want to delete a person") {
        Given("A mock up person in database")
        Await.result(personDaoImpl.createPerson(validPerson), 5.seconds)
        And("A expected result")
        val expected = (validPersonId, StatusCodes.OK, "Success")
        When("Delete a person")
        val result: Future[Either[ErrorException, (String, StatusCode, String)]] = personDaoImpl.deletePerson(validPersonId)
        whenReady(result) { rs =>
          Then("The result should be Delete Status")
          rs.right.get should be(expected)
        }
      }

      scenario("I want to delete a person, but it's fail by not found id") {
        Given("A id")
        val id = validPersonId
        And("Expected result")
        val expected = NotFoundException(errorMsg = Seq(s"Not found person by person_id: $id"))
        When("Delete a person")
        val result: Future[Either[ErrorException, (String, StatusCode, String)]] = personDaoImpl.deletePerson(id)
        whenReady(result) { rs =>
          Then("The result should be the same as expected")
          rs.left.get should be(expected)
        }
      }
    }
  }
}
