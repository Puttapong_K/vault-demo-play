package dao

import exception.NotFoundException
import models.Vault
import models.tables.VaultTable
import org.scalatest.BeforeAndAfterEach
import spec.CustomFeatureSpec
import testhelpers.{EvolutionHelper, Injector}

import scala.concurrent.Await
import scala.concurrent.duration._

class VaultDaoSpec extends CustomFeatureSpec with BeforeAndAfterEach {


  override def afterEach(): Unit = {
    EvolutionHelper.clean()
    EvolutionHelper.start()
  }

  trait VaultDaoTest {

    val vaultTable: VaultTable = Injector.inject[VaultTable]
    val vaultDaoImpl: VaultDaoImpl = Injector.inject[VaultDaoImpl]

    val validVault = Vault("id:1", "contextId", "Hmac")
    val validVaultSecond = Vault("id:2", "contextId", "Hmac")
  }

  new VaultDaoTest {
    feature("Create Vault") {
      scenario("I want to create Vault into database") {
        Given("A expect result")
        val expected = validVault
        When("Create vault")
        val result = vaultDaoImpl.createVault(validVault)
        whenReady(result) { rs =>
          Then("The result should be as expect")
          rs.right.get should be(expected)
        }
      }
    }

    feature("Get all Vaults from DB") {
      scenario("Get vaults, but it is empty") {
        When("Call get Vaults")
        val result = vaultDaoImpl.findAllVault
        whenReady(result) { rs =>
          Then("The result should be not found any vault")
          rs.left.get should be(NotFoundException(errorMsg = Seq("Not found any vaults")))
        }
      }

      scenario("Get vaults") {
        Given("A expected result")
        val expected = Seq(validVault)
        When("Call get Vaults")
        Await.result(vaultDaoImpl.createVault(validVault), 5.seconds)
        val result = vaultDaoImpl.findAllVault
        whenReady(result) { rs =>
          Then("The result should be list of vaults")
          rs.right.get should be(expected)
        }
      }
    }

    feature("Get Vaults by list(Id)") {
      scenario("Get vaults by list String of Ids, but database is empty") {
        Given("list of Id")
        val ids = Seq("id:1")
        And("A expected result")
        val expected = NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${ids.mkString(",")}"))
        When("Call GetVaultsByIds")
        val result = vaultDaoImpl.findVaultsByIds(ids)
        whenReady(result) { rs =>
          Then("The result should be Left( NotFound)")
          rs.left.get should be(expected)
        }
      }


      scenario("Get vaults by list String of Ids") {
        Given("A expected result")
        val expected = Seq(validVault, validVaultSecond)
        And("list of Id")
        val ids = Seq("id:1", "id:2")
        And("Mock data")
        Await.result(vaultDaoImpl.createVault(validVault), 5.seconds)
        Await.result(vaultDaoImpl.createVault(validVaultSecond), 5.seconds)
        When("Call GetVaultsByIds")
        val result = vaultDaoImpl.findVaultsByIds(ids)
        whenReady(result) { rs =>
          Then("The result should be Right(list of vaults)")
          rs.right.get should be(expected)
        }
      }
    }

    feature("get Vault by id") {
      scenario("I want to see vault data, but database is empty") {
        Given("A id")
        val id = "id:1"
        And("A expected result")
        val expected = NotFoundException(errorMsg = Seq(s"Not found vault by Id: ${id}"))
        When("Call get Vault by Id")
        val result = vaultDaoImpl.findVaultById(id)
        whenReady(result) { rs =>
          Then("The result should be Left(NotFound)")
          rs.left.get should be(expected)
        }
      }

      scenario("I want to see vault data") {
        Given("A id")
        val id = "id:1"
        And("A expected result")
        val expected = validVault
        And("Mock data")
        Await.result(vaultDaoImpl.createVault(validVault), 5.seconds)

        When("Call get Vault by Id")
        val result = vaultDaoImpl.findVaultById(id)
        whenReady(result) { rs =>
          Then("The result should be Right(Vault)")
          rs.right.get should be(expected)
        }
      }
    }
  }

}
