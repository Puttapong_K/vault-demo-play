package testhelpers

import play.api.db.DBApi
import play.api.db.evolutions.Evolutions

object EvolutionHelper {
  val dbapi = Injector.inject[DBApi]


  def clean(): Unit = Evolutions.cleanupEvolutions(dbapi.database("default"))


  def start(): Unit = Evolutions.applyEvolutions(dbapi.database("default"))


}