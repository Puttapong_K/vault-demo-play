package controllers


import models.{Person, PersonRequest, UUID}
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.Result
import play.api.test.Helpers._
import play.api.test._
import services.PersonServiceImpl
import spec.CustomFeatureSpec
import testhelpers.{EvolutionHelper, Injector}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class PersonControllerSpec extends CustomFeatureSpec with GuiceOneAppPerTest with BeforeAndAfterEach {

  override def afterEach(): Unit = {
    EvolutionHelper.clean()
    EvolutionHelper.start()
  }

  trait Controller {


    val personController: PersonController = Injector.inject[PersonController]
    val personService = Injector.inject[PersonServiceImpl]


    val personId = UUID.generateUUID("id:")

    val validPerson = Person(personId
      , "Ma"
      , 23
      , "Kanchanaburi"
      , "1212312121")


    val validPersonReq = PersonRequest(
      "Ma"
      , 23
      , "Kanchanaburi"
      , "1212312121")

  }

  new Controller {

    feature("CALL GET Persons") {
      scenario("Get persons, but there is no person in database") {
        val request = FakeRequest("GET", "/api/v1/persons").withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.findAllPersons.apply(request)
        status(result) should be(NOT_FOUND)
      }
      scenario("Get persons") {
        val request = FakeRequest("GET", "/api/v1/persons").withHeaders("Host" -> s"localhost:9000")
        Await.result(personService.createPerson(validPersonReq), 5.seconds)
        val result: Future[Result] = personController.findAllPersons.apply(request)
        status(result) should be(OK)
      }
    }


    feature("CALL GET a Person") {
      scenario("Get a person, but there is no person in database") {
        Given("A id")
        val id = personId
        val request = FakeRequest("GET", s"/api/v1/persons/${id}").withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.findPersonById(id).apply(FakeRequest())
        status(result) should be(NOT_FOUND)
      }
      scenario("Get person") {
        Given("mock person")
        val mockPerson = Await.result(personService.createPerson(validPersonReq), 5.seconds)
        And("A id")
        val id = mockPerson.right.get.personId
        val request = FakeRequest("GET", s"/api/v1/persons/${id}").withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.findPersonById(id).apply(request)
        status(result) should be(OK)
      }
    }

    feature("CALL POST Person") {
      scenario("Create a person") {
        Given("A json person")
        val jsonPersonRequest: JsValue = Json.toJson(PersonRequest(
          "Ma"
          , 23
          , "Kanchanaburi"
          , "1212312121"))
        When("Call create person")
        val request = FakeRequest("POST", s"/api/v1/persons/").withBody(jsonPersonRequest).withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.createPerson.apply(request)
        status(result) should be(CREATED)
      }

      scenario("Create a person, with bad Json") {
        Given("A json person")
        val jsonPersonRequest: JsValue = Json.parse(
          """
            |{
            |	"name": "Auzanaruk",
            |	"age": 23,
            |	"badJson": "BKK",
            |	"phoneNumber": "02345"
            |}
          """.stripMargin
        )
        When("Call create person")
        val request = FakeRequest("POST", s"/api/v1/persons/").withBody(jsonPersonRequest).withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.createPerson.apply(request)
        status(result) should be(BAD_REQUEST)
      }
    }


    feature("CALL DELETE PERSON") {

      scenario("DELETE PERSON") {

        Given("Mock data in database")
        val person = Await.result(personService.createPerson(validPersonReq), 5.seconds)
        And("A id")
        val id = person.right.get.personId
        val request = FakeRequest("GET", s"/api/v1/person/${id}").withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.deletePersonById(id).apply(request)
        status(result) should be(OK)
      }

      scenario("DELETE PERSON, but there is no person in database") {
        Given("A id")
        val id = personId
        val request = FakeRequest("GET", s"/api/v1/person/${id}").withHeaders("Host" -> s"localhost:9000")
        val result: Future[Result] = personController.deletePersonById(id).apply(request)
        status(result) should be(NOT_FOUND)
      }


    }
  }

}
