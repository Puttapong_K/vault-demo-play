# --- !Ups

CREATE TABLE person (
    person_id varchar(255)  PRIMARY KEY,
    name varchar(255) ,
    age integer ,
    address varchar(255),
    phone_number varchar(255)
);

CREATE TABLE vault (
    vault_id varchar(255)  PRIMARY KEY,
    contextid varchar(255),
    hmac varchar(255)
);


# --- !Downs

DROP TABLE person;
DROP TABLE vault;

