package spec

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen, Matchers}


/**
  * Created by puttapong on 22/9/2560.
  */
abstract class CustomFeatureSpec extends FeatureSpec
  with Matchers
  with GivenWhenThen
  with ScalaFutures
  with MockitoSugar