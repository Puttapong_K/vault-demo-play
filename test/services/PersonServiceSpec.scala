package services


import akka.http.scaladsl.model.StatusCodes
import com.scale360.vaultclient.VaultError
import com.scale360.vaultclient.secret.transit._
import dao.{PersonDao, VaultDao}
import exception.{InternalServerErrorException, NotFoundException}
import models.{Person, PersonRequest, UUID, Vault}
import org.mockito.Matchers._
import org.mockito.Mockito
import spec.CustomFeatureSpec

import scala.collection.immutable
import scala.concurrent.Future

class PersonServiceSpec extends CustomFeatureSpec {


  trait PersonServiceTest {

    val uuid: String = UUID.generateUUID("id:")
    val personId = uuid
    val validPerson = Person(personId
      , "Ma"
      , 23
      , "Kanchanaburi"
      , "1212312121")

    val validPersonReq = PersonRequest(
      "Ma"
      , 23
      , "Kanchanaburi"
      , "1212312121")

    val mockPerson: PersonDao = mock[PersonDao]

    val mockVaultDao = mock[VaultDao]
    val mockVaultService = mock[VaultService]


    val personService = new PersonServiceImpl(mockPerson, mockVaultService, mockVaultDao)

    Mockito.when(mockVaultService.makeCipherTextInputWithContext(anyString, anyString)).thenReturn(CiphertextInput(Ciphertext("ciphertext")))


    // Mock up function encodes (encryption, hash, and hashMac)
    Mockito.when(mockVaultService.encryptData(anyString, any[KeyName])).thenReturn(Right("contextId", Ciphertext("cipherText")))
    Mockito.when(mockVaultService.hashData(anyString)).thenReturn(Right(Hash("hashcode".getBytes())))
    Mockito.when(mockVaultService.hashMacData(anyString())).thenReturn(Right(Hmac("hmaccode")))
    Mockito.when(mockVaultService.verifyHMAC(anyString(), any[Hmac])).thenReturn(Right(true))

    // Mock up function decryption ( decryption, verify, )
    Mockito.when(mockVaultService.decryptBatchCipherText(any[immutable.Seq[CiphertextInput]], any[KeyName])).thenReturn(Right(immutable.Seq(Plaintext("Ma".getBytes()))))

    Mockito.when(mockVaultService.decryptCipherText(any[CiphertextInput], any[KeyName])).thenReturn(Right(Plaintext("Ma".getBytes())))


    // Mock up function get, create vault db
    Mockito.when(mockVaultDao.createVault(any[Vault])).thenReturn(Future.successful(Right(Vault(personId, "contextId", "ciphertext"))))
    Mockito.when(mockVaultDao.findVaultsByIds(any[Seq[String]])).thenReturn(Future.successful(Right(Seq(Vault(personId, "contextId", "ciphertext")))))
    Mockito.when(mockVaultDao.findVaultById(any[String])).thenReturn(Future.successful(Right(Vault(personId, "contextId", "ciphertext"))))


  }

  new PersonServiceTest {

    feature("Get persons") {
      scenario("get persons (Success)") {
        Given("A expects persons")
        val expectPersons = Seq(validPerson)
        And("Mock up person data")

        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Right(Seq(validPerson)))
        When("Call get persons")
        val result = personService.findAllPersons
        whenReady(result) { rs =>
          Then("The results should be Right(persons)")
          rs.right.get should be(expectPersons)
        }
      }


      scenario("get persons (Failure) by not found any vaults id") {
        Given("A expects persons")
        val expectPersons = NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${Seq(personId).mkString(",")}"))
        And("Mock up person data")

        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Right(Seq(validPerson)))
        Mockito.when(mockVaultDao.findVaultsByIds(any[Seq[String]])).thenReturn(Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${Seq(personId).mkString(",")}")))))
        When("Call get persons")
        val result = personService.findAllPersons
        whenReady(result) { rs =>
          Then("The results should be Left(Not found vaults by ids)")
          rs.left.get should be(expectPersons)
        }
      }


      scenario("get persons (Failure) by decryption batch fail") {
        Given("A expects persons")
        val expectPersons = InternalServerErrorException(errorMsg = Seq(VaultError.BadRequest(Nil).toString))
        And("Mock up person data")
        Mockito.when(mockVaultDao.findVaultsByIds(any[Seq[String]])).thenReturn(Future.successful(Right(Seq(Vault(personId, "contextId", "ciphertext")))))
        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Right(Seq(validPerson)))
        Mockito.when(mockVaultService.decryptBatchCipherText(any[immutable.Seq[CiphertextInput]], any[KeyName])) thenReturn (Left(VaultError.BadRequest(Nil)))
        When("Call get persons")
        val result = personService.findAllPersons
        whenReady(result) { rs =>
          Then("The results should be Left(Badrequest)")
          rs.left.get should be(expectPersons)
        }
      }


      scenario("get persons (Failure) by getVaults id from DB") {
        Given("A expects persons")
        val expectPersons = NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${Seq(personId).mkString(",")}"))
        And("Mock up person data")
        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Right(Seq(validPerson)))
        Mockito.when(mockVaultDao.findVaultsByIds(any[Seq[String]])).thenReturn(Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found vaults by Ids: ${Seq(personId).mkString(",")}")))))

        When("Call get persons")
        val result = personService.findAllPersons

        whenReady(result) { rs =>
          Then("The results should be Left(Vault Error)")
          rs.left.get should be(expectPersons)
        }
      }


      scenario("get persons (Failure) by verify Hmac") {
        Given("A expects persons")
        val expectPersons = InternalServerErrorException(errorMsg = Seq(VaultError.BadRequest(Nil).toString))
        And("Mock up person data")
        Mockito.when(mockVaultDao.findVaultsByIds(any[Seq[String]])).thenReturn(Future.successful(Right(Seq(Vault(personId, "contextId", "ciphertext")))))
        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Right(Seq(validPerson)))
        Mockito.when(mockVaultService.verifyHMAC(anyString(), any[Hmac])).thenReturn(Left(VaultError.BadRequest(Nil)))
        When("Call get persons")
        val result = personService.findAllPersons

        whenReady(result) { rs =>
          Then("The results should be Left(Vault Error)")
          rs.left.get should be(expectPersons)
        }
      }

      scenario("get persons (Failure)") {
        Given("A expects persons")
        val expectPersons = NotFoundException(errorMsg = Seq("Not found any persons"))
        And("Mock up person data")
        Mockito.when(mockPerson.findAllPersons) thenReturn Future.successful(Left(NotFoundException(errorMsg = Seq("Not found any persons"))))
        When("Call get persons")
        val result = personService.findAllPersons
        whenReady(result) { rs =>
          Then("The results should be Left(Not found any persons)")
          rs.left.get should be(expectPersons)
        }
      }
    }


    feature("Get a person") {
      scenario("get person (Success)") {
        Given("A expects person")
        val expectPersons = validPerson
        And("Mock up person data")
        Mockito.when(mockVaultService.verifyHMAC(anyString(), any[Hmac])).thenReturn(Right(true))
        Mockito.when(mockPerson.findPersonById(personId)) thenReturn Future.successful(Right(validPerson))
        When("Call get person")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Right(person)")
          rs.right.get should be(expectPersons)
        }
      }


      scenario("get persons (Failure) by not found vault id") {
        Given("A expects persons")
        val expectPerson = NotFoundException(errorMsg = Seq(s"Not found vault by Id: ${Seq(personId).mkString(",")}"))
        And("Mock up person data")

        Mockito.when(mockPerson.findPersonById(any[String])) thenReturn Future.successful(Right(validPerson))
        Mockito.when(mockVaultDao.findVaultById(any[String])).thenReturn(Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found vault by Id: ${personId}")))))
        When("Call get persons")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Left(Not found vault by id)")
          rs.left.get should be(expectPerson)
        }
      }


      scenario("get persons (Failure) by decryption  fail") {
        Given("A expects persons")
        val expectPersons = InternalServerErrorException(errorMsg = Seq(VaultError.BadRequest(Nil).toString))
        And("Mock up person data")
        Mockito.when(mockVaultDao.findVaultById(any[String])).thenReturn(Future.successful(Right(Vault(personId, "contextId", "ciphertext"))))
        Mockito.when(mockPerson.findPersonById(any[String])) thenReturn Future.successful(Right(validPerson))
        Mockito.when(mockVaultService.decryptCipherText(any[CiphertextInput], any[KeyName])) thenReturn (Left(VaultError.BadRequest(Nil)))
        When("Call get persons")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Left(Badrequest)")
          rs.left.get should be(expectPersons)
        }
      }


      scenario("get a person (Failure by verifyPhone Number)") {
        Given("A expects person")
        val expectPersons = InternalServerErrorException(errorMsg = Seq(VaultError.BadRequest(Nil).toString))
        And("Mock up person data")
        Mockito.when(mockPerson.findPersonById(personId)) thenReturn Future.successful(Right(validPerson))
        Mockito.when(mockVaultService.verifyHMAC(anyString(), any[Hmac])).thenReturn(Left(VaultError.BadRequest(Nil)))
        When("Call get persons")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Left(Vault Error)")
          rs.left.get should be(expectPersons)
        }
      }

      scenario("get a person (Failure by verifyPhone Number (false case)") {
        Given("A expects person")
        val expectPersons = InternalServerErrorException(errorMsg = Seq("Invalid phone number data had changed."))
        And("Mock up person data")
        Mockito.when(mockPerson.findPersonById(personId)) thenReturn Future.successful(Right(validPerson))
        Mockito.when(mockVaultService.verifyHMAC(anyString(), any[Hmac])).thenReturn(Right(false))
        When("Call get persons")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Left(Vault Error)")
          rs.left.get should be(expectPersons)
        }
      }

      scenario("get a person (Failure by verifyPhone Number Not found Vault_Id)") {
        Given("A expects person")
        val expectPersons = NotFoundException(errorMsg = Seq(s"Not found vault data by Id: ${personId}"))
        And("Mock up person data")
        Mockito.when(mockPerson.findPersonById(personId)) thenReturn Future.successful(Right(validPerson))
        Mockito.when(mockVaultDao.findVaultById(any[String])).thenReturn(Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found vault data by Id: ${personId}")))))

        When("Call get persons")
        val result = personService.findPersonById(personId)
        whenReady(result) { rs =>
          Then("The results should be Left(Not found vault data by Id)")
          rs.left.get should be(expectPersons)
        }
      }

      scenario("get a person (Failure)") {
        Given("A expects person")
        val expectPersons = NotFoundException(errorMsg = Seq(s"Not found a person by person_id: ${personId}"))
        And("Mock up person data")
        Mockito.when(mockPerson.findPersonById(personId)) thenReturn Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found a person by person_id: ${personId}"))))

        When("Call get persons")
        val result = personService.findPersonById(personId)

        whenReady(result) { rs =>
          Then("The results should be Left(Not found any persons)")
          rs.left.get should be(expectPersons)
        }
      }
    }


    feature("Delete person") {
      scenario("Delete a person (Success") {
        Given("A expect result")
        val expectResult = (uuid, StatusCodes.OK, "")
        And("A Mock up delete person")
        Mockito.when(mockVaultDao.deleteVaultById(any[String])).thenReturn(Future.successful(Right(expectResult)))
        Mockito.when(mockPerson.deletePerson(any[String])).thenReturn(Future.successful(Right(expectResult)))
        When("Delete a Person")
        val result = personService.deletePerson(personId)
        whenReady(result) { rs =>
          Then("result should be right (deleteSuccess)")
          rs.right.get should be(expectResult)
        }
      }

      scenario("Delete a person (Failure)") {
        Given("A expect result")
        val expectResult = NotFoundException(errorMsg = Seq(s"Not found person by person_id: ${personId}"))
        And("A Mock up delete person")
        Mockito.when(mockVaultDao.deleteVaultById(any[String])).thenReturn(Future.successful(Left(expectResult)))
        Mockito.when(mockPerson.deletePerson(any[String])).thenReturn(Future.successful(Left(NotFoundException(errorMsg = Seq(s"Not found person by person_id: ${personId}")))))
        When("Delete a Person")
        val result = personService.deletePerson(personId)
        whenReady(result) { rs =>
          Then("result should be Left(error message)")
          rs.left.get should be(expectResult)
        }
      }
    }
  }
}
