package services

import com.scale360.vaultclient
import com.scale360.vaultclient.{VaultConfig, VaultError, VaultToken}
import com.scale360.vaultclient.auth._
import com.scale360.vaultclient.client.ScalajHttpClient
import com.scale360.vaultclient.secret.transit._
import com.typesafe.config.{Config, ConfigFactory}
import models.VaultConfiguration
import spec.CustomFeatureSpec

import scala.collection.immutable
import scala.collection.parallel.immutable


class VaultServiceSpec extends CustomFeatureSpec {


  trait VaultServiceTest {

    val config = ConfigFactory.load()
    val vaultService = new VaultServiceImpl(config)


    //    // Get vault config details from Application
    val server = config.getString("vault.server")
    val version: Int = config.getInt("vault.version")
    val user = config.getString("vault.user")
    val password = config.getString("vault.password")
    val roleId = config.getString("vault.roleid")
    val secretId = config.getString("vault.secretid")
    val keyname = KeyName(config.getString("vault.keyname"))

    val invalidKey = KeyName("invalidKey")

  }


  new VaultServiceTest {

    feature("Vault configuration") {
      scenario("Vault configuration apply with Full condition") {
        val result = new VaultConfiguration(server = VaultConfig(server, version)
          , credentialByLogin = UserPassCredential(user, password)
          , credentialAppRole = AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId))
          , key = keyname)
        result.key should be(keyname)
        result.credentialByLogin should be(UserPassCredential(user, password))
        result.credentialAppRole should be(AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId)))
      }

      scenario("Vault configuration apply with User password credential") {
        val result = VaultConfiguration.apply(server = VaultConfig(server, version)
          , credentialByLogin = UserPassCredential(user, password)
          , keyName = keyname)
        result.key should be(keyname)
        result.credentialByLogin should be(UserPassCredential(user, password))
        result.credentialAppRole should not be (AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId)))

      }

      scenario("Vault configuration apply with AppRole credential") {
        val result = VaultConfiguration.apply(server = VaultConfig(server, version)
          , credentialAppRole = AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId))
          , keyName = keyname)
        result.key should be(keyname)
        result.credentialByLogin should not be (UserPassCredential(user, password))
        result.credentialAppRole should be(AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId)))
      }
    }

    feature("Authorized By authentication") {
      scenario("Success Authenticate by username, password") {
        Given("A valid username, password")
        val userPassCredential = UserPassCredential(user, password)
        When("authenticate")
        val result: Either[VaultError, AuthResponse] = vaultService.authorizedUser(userPassCredential)
        Then("The result should be Right(AuthResponse)")
        result.isRight should be(true)
      }

      scenario("Failure Authenticate by wrong username, and wrong password") {
        Given("A invalid username")
        val userPassCredential = UserPassCredential("user", "pass")
        When("authenticate")
        val result: Either[VaultError, AuthResponse] = vaultService.authorizedUser(userPassCredential)
        Then("The result should be Left(VaultError)")
        result.left.get should be(VaultError.BadRequest(Seq("invalid username or password").to[collection.immutable.Seq]))
      }

      scenario("Success Authenticate by RoleApp, SecretId") {
        Given("A valid appRole, secretId")
        val appRoleCrendential = AppRoleCredential(AppRoleCredential.RoleId(roleId), AppRoleCredential.SecretId(secretId))
        When("authenticate")
        val result: Either[VaultError, AuthResponse] = vaultService.authorizedByRole(appRoleCrendential)
        Then("The result should be Right(AuthResponse)")
        result.isRight should be(true)
      }

      scenario("Failure Authenticate by wrong Role, SecretId") {
        Given("A invalid roleId")
        val invalidRole = "invalidRole"
        And("A Approle Crendential")
        val appRoleCrendential = AppRoleCredential(AppRoleCredential.RoleId(invalidRole), AppRoleCredential.SecretId(secretId))
        When("authenticate")
        val result: Either[VaultError, AuthResponse] = vaultService.authorizedByRole(appRoleCrendential)
        Then("The result should be Left(VaultError)")
        result.isLeft should be(true)
      }
    }

    feature("Transit with Token") {
      scenario("connect transit with valid token") {
        val transit: TransitWithTokenFn = vaultService.transitWithToken(vaultService.tokenStore.getToken())
        Then("the transit should be encryption")
        val testResult = transit.encrypt(keyname)(PlaintextInput(Plaintext("abc".getBytes()), Some(Context("ab".getBytes))))
        testResult.isRight should be(true)
      }

      scenario("connect transit with invalid token") {
        val transit: TransitWithTokenFn = vaultService.transitWithToken(VaultToken(java.util.UUID.randomUUID()))
        Then("the transit should be encryption")
        val testResult = transit.encrypt(keyname)(PlaintextInput(Plaintext("abc".getBytes()), Some(Context("ab".getBytes))))
        testResult.left.get should be(VaultError.Unauthorized)
      }
    }

    feature("Make ciphertext input with context") {
      scenario("Make a ciphertextInput by cipherText, and context") {
        Given("a name ciphertext")
        val ct = "aa"
        When("Call the make ciphertextInput")
        val result = vaultService.makeCipherTextInputWithContext("ab", ct)
        Then("The result should be contain ciphertext")
        result.cipherText should be(Ciphertext(ct))
      }
    }

    feature("Encryption") {

      scenario("I want to encryption data to CipherText") {
        Given("A text")
        val text = "text"
        When("Call the encryption Data")
        val encryptResult: Either[VaultError, (vaultService.contextId, Ciphertext)] = vaultService.encryptData(text)

        Then("The result should return right")
        encryptResult.isRight should be(true)
      }

      scenario("I want to encryption data to CipherText with wrong keyName") {
        Given("A text")
        val text = "text"
        When("Call the encryption Data")
        val encryptResult: Either[VaultError, (vaultService.contextId, Ciphertext)] = vaultService.encryptData(text, invalidKey)
        Then("The result should return right")
        encryptResult.left.get should be(VaultError.Unauthorized)
      }

      scenario("I want to encryption list of plainText") {
        Given(" A list plaintext")
        val seqPlainText: Seq[(Plaintext, Context)] = Seq((Plaintext("a".getBytes()), Context("a".getBytes())), (Plaintext("b".getBytes()), Context("a".getBytes())))
        When("Call the encryption Data")
        val encryptResult: Either[VaultError, BatchResult[Ciphertext]] =
          vaultService.encryptionBatchPlainText(seqPlainText.to[collection.immutable.Seq])
        Then("The result should return Right( BatchResult)")
        encryptResult.isRight should be(true)
      }

      scenario("I want to encryption list of plainText with wrong keyName") {
        Given(" A list plaintext")
        val seqPlainText: Seq[(Plaintext, Context)] = Seq((Plaintext("a".getBytes()), Context("a".getBytes())), (Plaintext("b".getBytes()), Context("a".getBytes())))
        When("Call the encryption Data")
        val encryptResult: Either[VaultError, BatchResult[Ciphertext]] =
          vaultService.encryptionBatchPlainText(seqPlainText.to[collection.immutable.Seq], invalidKey)
        Then("The result should return Right( BatchResult)")
        encryptResult.isLeft should be(true)
      }
    }

    feature("Decryption") {
      Given("A mock up text and ciphertext")
      val text = "text"
      val ciphertext: (String, Ciphertext) = vaultService.encryptData(text).right.get

      scenario("I want to decryption ciphertext string to plaintext") {
        Given("A ciphertextInput")
        val ctInput: CiphertextInput = vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value)
        When("Call the decryption CipherText")
        val result: Either[VaultError, Plaintext] = vaultService.decryptCipherText(ctInput)
        Then("The result should return PlainText")
        result.isRight should be(true)
      }

      scenario("I want to decryption ciphertext string to plaintext with wrong keyName") {
        Given("A ciphertextInput")
        val ctInput: CiphertextInput = vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value)
        When("Call the decryption CipherText")
        val result: Either[VaultError, Plaintext] = vaultService.decryptCipherText(ctInput, invalidKey)
        Then("The result should return PlainText")
        result.isRight should be(true)
      }


      scenario("I want to decryption list of ciphertext string to list of plainText") {
        Given("A list of ciphertextInput")
        val seqCTInput: Seq[CiphertextInput] = Seq(vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value), vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value))
        When("Call the decryption CipherText")
        val result: Either[VaultError, vaultclient.Seq[Plaintext]] = vaultService.decryptBatchCipherText(seqCTInput.to[collection.immutable.Seq])
        Then("The result should return PlainText")
        result.isRight should be(true)
      }

      scenario("I want to decryption list of ciphertext string to list of plainText with wrong keyName") {
        Given("A list of ciphertextInput")
        val seqCTInput: Seq[CiphertextInput] = Seq(vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value), vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value))
        When("Call the decryption CipherText")
        val result: Either[VaultError, vaultclient.Seq[Plaintext]] = vaultService.decryptBatchCipherText(seqCTInput.to[collection.immutable.Seq], invalidKey)
        Then("The result should return PlainText")
        result.left.get should be(VaultError.Unauthorized)
      }
    }

    feature("Hash data") {
      scenario("I want to change data to hash data") {
        Given("A text")
        val text = "Text"
        When("Call the hash function")
        val result: Either[VaultError, Hash] = vaultService.hashData(text)
        Then("The result should return Right(hashData)")
        result.isRight should be(true)
      }
    }

    feature("Hash Mac, and verify hmac ") {
      Given("A text")
      val text = "Text"
      scenario("I want to change data to hmac ") {
        When("Call the hash-mac function")
        val result = vaultService.hashMacData(text)
        Then("the result should return Right(hmacData)")
        result.isRight should be(true)
      }

      scenario("I want to valid data to hmac") {
        Given("A hmac data")
        val hmac = vaultService.hashMacData(text).right.get
        When("Call the verify hmac")
        val result = vaultService.verifyHMAC(text, hmac)
        Then("the result should return true")
        result.right.get should be(true)
      }


      scenario("I want to valid data to hmac with data has change") {
        Given("A hmac data")
        val hmac = vaultService.hashMacData(text).right.get
        And("A new data")
        val newText = "newText"
        When("Call the verify hmac")
        val result = vaultService.verifyHMAC(newText, hmac)
        Then("the result should return true")
        result.right.get should be(false)
      }
    }

    feature("Renew ciphertext value") {
      Given("A mock up ciphertext")
      val text = "text"
      val ciphertext: (String, Ciphertext) = vaultService.encryptData(text).right.get
      scenario("I want to re new ciphertext value to new ciphertext") {
        Given("A ciphertextInput")
        val ctInput: CiphertextInput = vaultService.makeCipherTextInputWithContext(ciphertext._1, ciphertext._2.value)
        When("Call the rewrap CipherText")
        val result: Either[VaultError, Ciphertext] = vaultService.renewCipherText(ctInput)
        Then("The result should return PlainText")
        result.right.get.value should not be ciphertext._2.value
      }
    }
  }
}
