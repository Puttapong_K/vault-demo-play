
name := """vault-demo-play"""
organization := "com.example"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)


routesGenerator := InjectedRoutesGenerator


resolvers ++= Seq("Artifactory Realm" at "http://repo.dev.dotography.net/artifactory/sbt-lib-release")
scalaVersion := "2.12.3"

credentials += Credentials("Artifactory Realm", "repo.dev.dotography.net", "deployer", "deployer")

libraryDependencies += guice
libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % "test",
  specs2 % Test,
  guice,
  "com.scale360" %% "vault-client" % "1.2",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "mysql" % "mysql-connector-java" % "5.1.36",
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  "org.slf4j" % "slf4j-nop" % "1.7.25",
  "com.h2database" % "h2" % "1.4.194",
  "com.google.inject" % "guice" % "4.0",
  "org.mockito" % "mockito-all" % "1.10.19" % "test",
  "com.scale360" %% "response-mask" % "1.0"
)

coverageEnabled := true
parallelExecution in Test := false



coverageExcludedPackages := "utils.http.*;router.*;controllers.Assets.*;controllers.javascript.*;conf.*;<empty>;filters"
